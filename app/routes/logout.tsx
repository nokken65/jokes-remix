import type { ActionFunction, LoaderFunction } from "@remix-run/node";
import { redirect } from "@remix-run/node";

import { logout } from "~/utils/session.server";

const action: ActionFunction = async ({ request }) => {
  return logout(request);
};

const loader: LoaderFunction = async () => {
  return redirect("/");
};

export { action, loader };
